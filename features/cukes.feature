Feature: Login with Abril ID
	As a Abril ID user
	I want to login with Abril ID

Scenario: Login with Abril ID
	When I visit the Abril ID site
	And I fill the email field with "daenerys@mailinator.net"
	And I fill the password field with "bolinha"
	And I press the Entrar button
	Then I should see the message "Os dados de login estão incorretos."
