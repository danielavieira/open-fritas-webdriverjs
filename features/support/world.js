var webdriverjs = require('webdriverjs');

 //env variables
 var browserName = process.env.BROWSER ||'chrome';
 var plataform = process.env.PLATAFORM ||'WINDOWS'
 var version = process.env.BROWSER_VERSION ||'';
 var gridService = process.env.GRID ||'browserStack';

 //hubs
 var hubs = {
    browserStack: 'hub.browserstack.com',
	testingBot: 'hub.testingbot.com',
	sauceLabs: 'ondemand.saucelabs.com'
 };

 //portas para os grids
 var ports = {
  browserStack: 4444,
  testingBot: 4444,
  sauceLabs: 80
 };

 var caps = {
  testingBot: {
    'browserName': browserName,
    'version': version,
    'platform': plataform,
    'api_key': '6056c7d6acea1c7de329d78b6c14c75f', 
    'api_secret': 'f68f7ec142306aaf9fa58ddbdf4409e4'
  },
  sauceLabs: {
    'browserName': browserName,
    'version': version,
    'platform': plataform,
    'username': 'karolineleite',
    'accessKey': 'b6ae7dd9-472b-40a3-86ff-f6e4e6b7ee80'
  },  
  browserStack: {
    'browserName': browserName,
    'version': version,
    'platform': plataform,
    'browserstack.user' : 'TestesdoServico', 
    'browserstack.key': 'KXhJa4coi54TAsus1msb',
    'browserstack.tunnel': true,
    'browserstack.debug': true
  }
 };

 var World = function(callback) {
   this.browser = webdriverjs.remote({
                                  'host': hubs[gridService],  
                                  'port': ports[gridService],
                                  'desiredCapabilities': caps[gridService]
                               });

   this.visit = function(url, callback) {
     this.browser.init().url(url, callback);
   };

   
   callback(); // tell Cucumber we're finished and to use 'this' as the world instance
 };

 exports.World = World;