
var aTest = function () {
	this.World = require("../support/world.js").World;


this.When(/^I visit the Abril ID site$/, function(callback) {
	this.visit('http://id.abril.com.br', callback);
});

this.When(/^I fill the email field with "([^"]*)"$/, function(email, callback) {
	this.browser.setValue('#login', email, callback);
});

this.When(/^I fill the password field with "([^"]*)"$/, function(senha, callback) {
	this.browser.setValue('#senha', senha, callback);
});

this.When(/^I press the Entrar button$/, function(callback) {
	this.browser.click('input#loginEnviar', callback);
});

this.Then(/^I should see the message "([^"]*)"$/, function(mensagem, callback) {
	this.browser.getText('#loginerror', function(err, txt){
		if (txt.indexOf(mensagem) != -1){
			callback();
		}
		else{
			callback.fail(new Error("Seu teste nao deu certo, sorry! Mensagem exibida: "+txt));
		}

	});
});


};

module.exports = aTest;
